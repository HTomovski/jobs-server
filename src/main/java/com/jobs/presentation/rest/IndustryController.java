package com.jobs.presentation.rest;

import com.jobs.model.Industry;
import com.jobs.service.IndustryService;
import java.util.List;
import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Created by HTomovski Date: 09-Sep-19 Time: 1:42 PM */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/industries")
public class IndustryController {

  private final IndustryService industryService;

  public IndustryController(IndustryService industryService) {
    this.industryService = industryService;
  }

  @GetMapping
  public List<Industry> getIndustries() {
    return industryService.getIndustries();
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
  @PostMapping("/{industry-name}")
  public Industry createIndustryPathVariable(@PathVariable("industry-name") String industryName) {
    return industryService.createIndustry(industryName);
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
  @PostMapping("/create")
  public Industry createIndustry(@NotNull @RequestBody String industryName) {
    return industryService.createIndustry(industryName);
  }
}
