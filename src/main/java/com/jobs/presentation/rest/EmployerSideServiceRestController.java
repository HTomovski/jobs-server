package com.jobs.presentation.rest;

import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.JobOffer;
import com.jobs.model.User;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.model.views.JobOfferView;
import com.jobs.service.JobOfferService;
import com.jobs.service.UserService;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/** Created by HTomovski Date: 08-May-19 Time: 2:42 PM */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/employer")
public class EmployerSideServiceRestController {
  private final UserService userService;
  private final JobOfferService jobOfferService;

  public EmployerSideServiceRestController(UserService service, JobOfferService jobOfferService) {
    this.userService = service;
    this.jobOfferService = jobOfferService;
  }

  /*
      @GetMapping("/clients/skill/{skillId}")
      public List<User> getUsersBySkill(@PathVariable(name = "skillId") Long skillId) {
          return userService.findAllBySkill(skillId);
      }
  */
  @GetMapping("/clients/{clientId}")
  public User getClientById(@PathVariable(name = "clientId") Long userId) {
    Optional<User> user = userService.findById(userId);
    if (user.isPresent()) return user.get();
    throw new NoSuchUserException();
  }

  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @PostMapping(value = "/job-offers", consumes = MediaType.APPLICATION_JSON_VALUE)
  public JobOffer createJobOffer(
      @RequestBody JobOffer jobOffer, @CurrentUser UserPrincipal userPrincipal) {
    return jobOfferService.createJobOffer(jobOffer, userPrincipal);
  }

  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @PatchMapping(value = "/job-offers", consumes = MediaType.APPLICATION_JSON_VALUE)
  public JobOffer editJobOffer(
      @RequestBody JobOffer jobOffer, @CurrentUser UserPrincipal userPrincipal) {
    // todo: Forbid access to any user that is not the creator of the job offer
    return jobOfferService.editJobOffer(jobOffer, userPrincipal);
  }

  // job offerite za eden user se vrakjaat i od jobOfferController!
  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @GetMapping("/job-offers/{employerId}/{page}")
  public Page<JobOfferView> viewMyJobOffers(
      @PathVariable("employerId") Long employerId, @PathVariable("page") int page) {
    // todo: Forbid access to all users except the 'employerId'
    return jobOfferService.viewJobOffersByUser(employerId, page);
  }

  @DeleteMapping("/job-offers/{id}")
  public void closeJobOffer(
      @PathVariable("id") Long jobOfferId, @CurrentUser UserPrincipal userPrincipal) {
    // todo: Forbid access to all users except the 'employerId'
    jobOfferService.closeJobOffer(jobOfferId, userPrincipal);
  }
}
