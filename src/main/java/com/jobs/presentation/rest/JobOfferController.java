package com.jobs.presentation.rest;

import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.JobOffer;
import com.jobs.model.exceptions.NoSuchJobOfferException;
import com.jobs.model.views.JobOfferView;
import com.jobs.service.JobOfferService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/** Created by HTomovski Date: 19-Oct-19 Time: 7:46 PM */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/job-offers")
public class JobOfferController {

  private final JobOfferService jobOfferService;

  public JobOfferController(JobOfferService jobOfferService) {
    this.jobOfferService = jobOfferService;
  }

  // === Create ===
  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public JobOffer createJobOffer(
      @RequestBody JobOffer jobOffer, @CurrentUser UserPrincipal userPrincipal) {
    return jobOfferService.createJobOffer(jobOffer, userPrincipal);
  }

  // === Edit ===
  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public JobOffer editJobOffer(
      @RequestBody JobOffer jobOffer, @CurrentUser UserPrincipal userPrincipal) {
    return jobOfferService.editJobOffer(jobOffer, userPrincipal);
  }

  // === Delete ===
  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @DeleteMapping("/{id}")
  public void closeJobOffer(
      @PathVariable("id") Long jobOfferId, @CurrentUser UserPrincipal userPrincipal) {
    System.out.printf("Deletion for job %d started.\n", jobOfferId);
    System.out.printf("Current user id is %d.\n", userPrincipal.getId());
    jobOfferService.closeJobOffer(jobOfferId, userPrincipal);
  }

  // === Search ===
  @GetMapping(value = "/simple-search")
  public Page<JobOfferView> browseByMultipleMixedFields(
      @RequestParam("searchParams") List<String> searchParams, @RequestParam("page") int page) {
    System.out.println(searchParams);

    return jobOfferService.browseJobOffersByMultipleMixedFields(searchParams, page);
  }


  @GetMapping("/advanced-old-querystring")
  //    public Page<JobOfferView> browseByMultipleSeparateFieldsBody(
  public Page<JobOfferView> browseByMultipleSeparateFieldsQueryString(
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "location", required = false) String location,
      @RequestParam(value = "empName", required = false) String empName,
      @RequestParam(value = "desc", required = false) String desc,
      @RequestParam(value = "skills", required = false) List<Long> skills,
      @RequestParam("page") int page) {
    System.out.println("name: " + name);
    System.out.println("location: " + location);
    System.out.println("empName: " + empName);
    System.out.println("desc: " + desc);
    System.out.println("skills: " + skills);

    return jobOfferService
        .browseJobOffersByMultipleSeparateFields(name, location, empName, desc, skills, page);
    //return jobOfferService.getJobOffersDynamic(name, location, empName, desc, skills);
  }

  @PostMapping("/advanced-old")
  //    public Page<JobOfferView> browseByMultipleSeparateFields(
  public Page<JobOfferView> browseByMultipleSeparateFields(
      @RequestBody JobOfferView filterCriteria,
      @RequestParam("page") int page) {
    System.out.println("filterCriteriaJob: " + filterCriteria);

     return jobOfferService
         .browseJobOffersByMultipleSeparateFieldsFc(filterCriteria, page);
    //return jobOfferService.getJobOffersDynamic(name, location, empName, desc, skills);
  }

  @PostMapping("/advanced-example/{page}")
  public Page<JobOfferView> browseByMultipleSeparateFieldsPost(
      @RequestBody JobOfferView exampleJobOffer, @PathVariable(name = "page") int page) {
    return jobOfferService
        .browseJobOffersByMultipleSeparateFieldsFc(exampleJobOffer, page);
   // return jobOfferService.findJobOfferViewsByExample(exampleJobOffer, page);
  }

  @GetMapping("/skills")
  public Page<JobOfferView> browseJobOffersBySkillsQueryString(
      @RequestParam(name = "skillIds") String skillIds, @RequestParam(name = "page") int page) {
    System.out.println(skillIds);
    System.out.println(page);
    String[] skillIdsStrings = skillIds.split(",");
    List<Long> skillIdsList = new ArrayList<>();
    for (String skillIdsString : skillIdsStrings) {
      if (skillIdsString.equals("")) continue;
      skillIdsList.add(Long.parseLong(skillIdsString));
    }
    return jobOfferService.customFindAllByRequiredSkillsPage(skillIdsList, page);
  }

  @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
  @GetMapping("/employer/{employerId}/{page}")
  public Page<JobOfferView> viewJobOffersByUser(
      @PathVariable("employerId") Long employerId, @PathVariable("page") int page) {
    return jobOfferService.viewJobOffersByUser(employerId, page);
  }

  @GetMapping("/{jobId}/")
  public JobOffer getJobOfferById(@PathVariable(name = "jobId") Long jobOfferId) {
    Optional<JobOffer> jobOffer = jobOfferService.viewJobOffer(jobOfferId);
    if (jobOffer.isPresent()) return jobOffer.get();
    else throw new NoSuchJobOfferException();
  }

  @GetMapping("/test-search")
  public List<Long> testSearch() {
    return jobOfferService.testSearch();
  }
}
