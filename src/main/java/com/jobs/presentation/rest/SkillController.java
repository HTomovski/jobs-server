package com.jobs.presentation.rest;

import com.jobs.model.Skill;
import com.jobs.model.payloads.requests.CreateSkillRequest;
import com.jobs.service.SkillService;
import java.util.List;
import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Created by HTomovski Date: 08-Sep-19 Time: 9:58 AM */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/skills")
public class SkillController {

  private final SkillService skillService;

  public SkillController(SkillService skillService) {
    this.skillService = skillService;
  }

  @GetMapping("/{id}")
  public List<Skill> getSkillsByIndustry(@PathVariable(name = "id") Long industryId) {
    return skillService.getSkillsByIndustryId(industryId);
  }

  @PostMapping("/create")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
  public Skill createSkill(@NotNull @RequestBody CreateSkillRequest createSkillRequest) {
    return skillService.createSkill(createSkillRequest.skillName, createSkillRequest.industryId);
  }
}
