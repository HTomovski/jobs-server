package com.jobs.presentation.rest;

import com.jobs.config.security.CurrentUser;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Candidate;
import com.jobs.model.Employer;
import com.jobs.model.User;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.model.payloads.requests.LoginRequest;
import com.jobs.model.payloads.requests.RegisterRequest;
import com.jobs.model.payloads.responses.ApiResponse;
import com.jobs.service.AuthenticationService;
import com.jobs.service.UserService;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/** Created by HTomovski Date: 08-Sep-19 Time: 9:58 AM */
@RestController
@CrossOrigin({"*", "localhost:3000"})
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;
  private final AuthenticationService authenticationService;

  // LOGIN
  @PostMapping(value = "/login")
  public ResponseEntity<?> loginUser(
      // @Valid
      @RequestBody
      LoginRequest loginRequest) {

    return ResponseEntity.ok(authenticationService.authenticate(loginRequest));
  }

  // REGISTER
  @PostMapping(value = "/register/candidate")
  public ResponseEntity<?> newCandidate(@RequestBody RegisterRequest registerRequest) {
    Candidate candidate =
        userService.saveCandidate(
            registerRequest.getName(), registerRequest.getEmail(), registerRequest.getPassword());
    URI location =
        ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/users")
            .buildAndExpand()
            .toUri();
    return ResponseEntity.created(location).body(new ApiResponse(true, candidate.id.toString()));
  }

  @PostMapping(value = "/register/employer")
  public ResponseEntity<?> newEmployer(@RequestBody RegisterRequest registerRequest) {
    Employer employer =
        userService.saveEmployer(
            registerRequest.getName(), registerRequest.getEmail(), registerRequest.getPassword());
    URI location =
        ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/users")
            .buildAndExpand()
            .toUri();
    return ResponseEntity.created(location).body(new ApiResponse(true, employer.id.toString()));
  }

  // GET USER BY ID
  @GetMapping("/{userId}")
  public User getUserById(@PathVariable(name = "userId") Long userId) {
    Optional<Candidate> candidateOptional = userService.findCandidateById(userId);
    if (candidateOptional.isPresent()) return candidateOptional.get();
    Optional<Employer> employerOptional = userService.findEmployerById(userId);
    if (employerOptional.isPresent()) return employerOptional.get();
    Optional<User> user = userService.findById(userId);
    return user.orElseThrow(NoSuchUserException::new);
  }

  // go vrakja userot duri i ako e tokenot expired
  @GetMapping("/me")
//  @PreAuthorize("hasAnyRole('ROLE_USER, ROLE_ADMIN')")
  public UserPrincipal getCurrentUserPrincipal(
      @CurrentUser UserPrincipal user,
      @CurrentSecurityContext(expression = "authentication") Authentication authentication) {
    //        Optional<Candidate> candidateOptional = userService.findCandidateById(user.getId());
    //        if (candidateOptional.isPresent())
    //            return candidateOptional.get();
    //        Optional<Employer> employerOptional = userService.findEmployerById(user.getId());
    //        if (employerOptional.isPresent())
    //            return employerOptional.get();
    //        return userService.findById(user.getId()).orElseThrow(NoSuchUserException::new);

    System.out.println(authentication);
    System.out.println(user);
    return user;
  }

  @GetMapping("/skill/{skillId}/{pageNum}")
  public Page<Candidate> getCandidatesBySkill(
      @PathVariable("skillId") Long skillId, @PathVariable("pageNum") int pageNum) {
    return userService.findAllBySkill(skillId, pageNum);
  }

  @GetMapping("/skills/{skillIds}")
  public List<Candidate> getCandidatesBySkills(@PathVariable("skillIds") List<Long> skillIds) {
    return userService.findAllBySkills(skillIds);
  }

  @PatchMapping()
  public User updateUser(@RequestBody User user) {
    return userService.save(user);
  }

  @PatchMapping("/candidate")
  public Candidate updateCandidate(@RequestBody Candidate candidate) {
    return userService.updateCandidate(candidate);
  }

  @PatchMapping("/employer")
  public Employer updateEmployer(@RequestBody Employer employer) {
    return userService.updateEmployer(employer);
  }
}
