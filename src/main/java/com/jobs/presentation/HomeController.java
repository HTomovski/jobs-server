package com.jobs.presentation;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/** Created by HTomovski Date: 22-Aug-43 Time: 5:13 PM */
@Controller
public class HomeController {
  @GetMapping(value = "/home")
  public void getHomepage(HttpServletResponse response) throws IOException {
    PrintWriter pw = response.getWriter();
    response.setContentType("text/html");
    pw.println("<html>");
    pw.println("<body>");

    pw.println("<h1>");
    pw.println("ThiS iS My hOMe.");
    pw.println("</h1>");
    pw.println("</body>");
    pw.println("</html>");
  }
}
