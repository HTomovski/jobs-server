package com.jobs.service;

import com.jobs.model.*;
import java.util.Optional;

/** Created by HTomovski Date: 15-Jul-19 Time: 6:30 PM */
public interface AdminSideService {

  // Skill
  Skill createNewSkill(String skillName, Industry industry);

  Skill updateSkill(Long skillId, String newSkillName);

  Optional<Skill> viewSkill(String skillName);

  Optional<Skill> viewSkill(Long skillId);

  void removeSkill(Long skillId);

  // Client
  User createNewUser(User user);

  User updateUser(User user);

  Optional<User> viewUser(Long userId);

  void removeUser(Long userId);

  // JobOffer
  JobOffer createNewJobOffer(JobOffer jobOffer);

  JobOffer updateJobOffer(JobOffer jobOffer);

  Optional<JobOffer> viewJobOffer(Long jobOfferId);

  void removeJobOffer(Long jobOfferId);
}
