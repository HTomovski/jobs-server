package com.jobs.service;

import com.jobs.model.Skill;
import java.util.List;

/**
 * Created by HTomovski
 * Date: 09-Sep-19
 * Time: 9:39 AM
 */
public interface SkillService {

    List<Skill> getSkillsByIndustryId(Long industryId);

    Skill createSkill(String skillName, Long industryId);
}
