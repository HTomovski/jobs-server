package com.jobs.service;

import com.jobs.model.payloads.requests.LoginRequest;
import com.jobs.model.payloads.responses.JwtAuthenticationResponse;

public interface AuthenticationService {

  JwtAuthenticationResponse authenticate(LoginRequest loginRequest);
}
