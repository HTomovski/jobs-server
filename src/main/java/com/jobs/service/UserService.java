package com.jobs.service;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Candidate;
import com.jobs.model.Employer;
import com.jobs.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/** Created by HTomovski Date: 22-Aug-19 Time: 3:13 PM */
public interface UserService extends UserDetailsService {

  Optional<User> findByEmail(String email);

  Page<User> findAll(
      UserPrincipal currentLoggedInUser,
      Pageable pageable,
      String name,
      String email,
      String branchName);

  UserDetails findUserById(Long id);

  Optional<User> findById(Long userId);

  Optional<Candidate> findCandidateById(Long userId);

  Optional<Employer> findEmployerById(Long userId);

  User save(String name, String email, String password);

  Candidate saveCandidate(String name, String email, String password);

  Employer saveEmployer(String name, String email, String password);

  User save(User user);

  Candidate updateCandidate(Candidate candidate);

  Employer updateEmployer(Employer employer);

  Page<Candidate> findAllBySkill(Long skillId, int pageNum);

  List<Candidate> findAllBySkills(List<Long> skillIds);
}
