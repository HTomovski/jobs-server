package com.jobs.service;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.JobOffer;
import com.jobs.model.views.JobOfferView;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

/**
 * Created by HTomovski
 * Date: 27-Oct-19
 * Time: 7:51 AM
 */
public interface JobOfferService {

    Optional<JobOffer> viewJobOffer(Long jobOfferId);

    JobOffer createJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal);

    JobOffer editJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal);

    void closeJobOffer(Long jobOfferId, UserPrincipal userPrincipal);

    Page<JobOfferView> findJobOfferViewsByExample(JobOfferView exampleJobOffer, int page);

  Page<JobOfferView> browseJobOffersByMultipleSeparateFields(
      String name, String location, String empName, String desc, List<Long> skillIds, int page);

  Page<JobOfferView> browseJobOffersByMultipleSeparateFieldsFc(
      JobOfferView filterCriteria, int page);

    Page<JobOfferView> browseJobOffersByMultipleMixedFields(List<String> propertiesString, int page);

    Page<JobOfferView> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page);

    Page<JobOfferView> viewJobOffersByUser(Long employerId, int page);

  List<JobOffer> getJobOffersDynamic(String name,
                                     String location,
                                     String empName,
                                     String desc,
                                     List<Long> skillIds);

  List<Long> testSearch();
}
