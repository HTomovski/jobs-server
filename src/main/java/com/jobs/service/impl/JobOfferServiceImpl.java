package com.jobs.service.impl;

import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Employer;
import com.jobs.model.JobOffer;
import com.jobs.model.utils.Ranking;
import com.jobs.model.Skill;
import com.jobs.model.exceptions.NoSuchEmployerException;
import com.jobs.model.views.JobOfferView;
import com.jobs.persistence.*;
import com.jobs.persistence.specifications.JobOfferSpecification;
import com.jobs.persistence.specifications.criteria.SearchCriteria;
import com.jobs.persistence.specifications.operations.SearchOperation;
import com.jobs.service.JobOfferService;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

/** Created by HTomovski Date: 27-Oct-19 Time: 7:55 AM */
@Service
public class JobOfferServiceImpl implements JobOfferService {

  private final JobOfferJpaRepository jobOfferJpaRepository;
  private final JobOfferViewJpaRepository jobOfferViewJpaRepository;
  private final SkillJpaRepository skillJpaRepository;
  private final UserViewJpaRepository userViewJpaRepository;
  private final EmployerJpaRepository employerJpaRepository;

  @Value("${page-size}")
  private int PAGE_SIZE;

  public JobOfferServiceImpl(
      JobOfferJpaRepository jobOfferJpaRepository,
      JobOfferViewJpaRepository jobOfferViewJpaRepository,
      SkillJpaRepository skillJpaRepository,
      UserViewJpaRepository userViewJpaRepository,
      UserJpaRepository userJpaRepository,
      EmployerJpaRepository employerJpaRepository,
      CandidateJpaRepository candidateJpaRepository) {
    this.jobOfferJpaRepository = jobOfferJpaRepository;
    this.jobOfferViewJpaRepository = jobOfferViewJpaRepository;
    this.skillJpaRepository = skillJpaRepository;
    this.userViewJpaRepository = userViewJpaRepository;
    this.employerJpaRepository = employerJpaRepository;
  }

  @Override
  public Optional<JobOffer> viewJobOffer(Long jobOfferId) {
    return jobOfferJpaRepository.findById(jobOfferId);
  }

  @Override
  public JobOffer createJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal) {

    Employer employer = jobOffer.getEmployer();
    System.out.println(employer.id);
    System.out.println(userPrincipal.getId());

    if (employer.id.equals(userPrincipal.getId())) {
      employerJpaRepository
          .findById(employer.id)
          .ifPresent(jobOffer::setEmployer);

      if (jobOffer.getEmployer() == null) throw new NoSuchEmployerException();

      return jobOfferJpaRepository.save(jobOffer);
    }

    throw new NoSuchEmployerException();
  }

  @Override
  public JobOffer editJobOffer(JobOffer jobOffer, UserPrincipal userPrincipal) {
    // Todo: decide if a check with the existing job offer is needed
    //  example scenario: an employer that's not the creator of a job offer is trying to edit it
    //  by inserting his id on the new job offer object with the edits.
    Optional<JobOffer> existingJobOpt = jobOfferJpaRepository.findById(jobOffer.getId());
    if (jobOffer.getEmployer().id.equals(userPrincipal.getId()))
      return jobOfferJpaRepository.save(jobOffer);
    else {
      System.out.println("=== JOB CREATOR AND CURRENT USER ID MISMATCH IN EDIT SAVE ===");
      return null;
    }
  }

  @Override
  public void closeJobOffer(Long jobOfferId, UserPrincipal userPrincipal) {
    JobOfferView jobOfferView = jobOfferViewJpaRepository.getById(jobOfferId);
    if (jobOfferView.getEmployer().getId().equals(userPrincipal.getId()))
      jobOfferJpaRepository.deleteById(jobOfferId);
    else {
      System.out.println("=== JOB CREATOR AND CURRENT USER ID MISMATCH IN JOB DELETE ===");
    }
  }

  @Override
  public Page<JobOfferView> findJobOfferViewsByExample(JobOfferView exampleJobOffer, int page) {
    ExampleMatcher exampleMatcher =
        ExampleMatcher.matchingAll().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
    return jobOfferViewJpaRepository.findAll(
        Example.of(exampleJobOffer, exampleMatcher), PageRequest.of(page, PAGE_SIZE));
  }

  @Override
  public Page<JobOfferView> browseJobOffersByMultipleSeparateFields(
      String name, String location, String empName, String desc, List<Long> skillIds, int page) {

    JobOfferView jobOfferView = new JobOfferView();
    jobOfferView.setName(name);
    jobOfferView.setLocation(location);
    if (empName != null && !empName.trim().isEmpty())
      jobOfferView.setEmployer(this.userViewJpaRepository.findByNameContaining(empName));
    jobOfferView.shortJobDescription = desc;
    if (skillIds != null && skillIds.size() > 0) {
      jobOfferView.addRequiredSkills(
          skillIds.stream()
              .map(skillId -> this.skillJpaRepository.findById(skillId).orElse(null))
              .filter(Objects::nonNull)
              .collect(Collectors.toList()));
    }

    System.out.println("exampleJob.employer: ");
    System.out.println(jobOfferView.getEmployer());
    System.out.println("exampleJob.skills: ");
    System.out.println(jobOfferView.getRequiredSkills());
    ExampleMatcher exampleMatcher =
        ExampleMatcher.matchingAny().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

    Example<JobOfferView> example = Example.of(jobOfferView, exampleMatcher);

    // komplikacii nepotrebni
    List<JobOfferView> jobs = this.jobOfferViewJpaRepository.findAll(example);

    // todo: possibly figure out a better way to add the skills to the query
    // (query by example with a list attribute)
    if (jobOfferView.getRequiredSkills() != null) {

      // todo: check wtf is going on here, also see previous commit.
      jobs.removeIf(
          job ->
              !new HashSet<>(job.getRequiredSkills())
                  .containsAll(jobOfferView.getRequiredSkills()));
    }

    int startIndex = page * PAGE_SIZE, endIndex = (page + 1) * PAGE_SIZE;
    if (startIndex > jobs.size()) return null;
    if (endIndex > jobs.size()) {
      endIndex = jobs.size();
    }
    Page<JobOfferView> pageToReturn =
        new PageImpl<JobOfferView>(
            jobs.subList(startIndex, endIndex), PageRequest.of(page, PAGE_SIZE), jobs.size());

    System.out.println(pageToReturn);
    return pageToReturn;
  }

  @Override
  public Page<JobOfferView> browseJobOffersByMultipleSeparateFieldsFc(
      JobOfferView filterCriteria, int page) {

    System.out.println(filterCriteria);

    ExampleMatcher exampleMatcher = ExampleMatcher
        .matchingAny()
        .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
        .withIgnoreNullValues();

    Example<JobOfferView> example = Example.of(filterCriteria, exampleMatcher);
    return jobOfferViewJpaRepository.findAll(example, PageRequest.of(page, PAGE_SIZE));
  }

  @Override
  public Page<JobOfferView> browseJobOffersByMultipleMixedFields(
      List<String> propertiesList, int page) {

    List<JobOfferView> jobOffers = new ArrayList<>();

    // Querying the database for each parameter, and removing duplicates
    for (String property : propertiesList) {
      jobOffers.addAll(jobOfferViewJpaRepository.customFindAllBySingleUnknownField(property));
    }
    jobOffers = jobOffers.stream().distinct().collect(Collectors.toList());
    jobOffers.forEach(System.out::println);

    // Ranking the results by relevance
    List<Ranking<JobOfferView>> rankings =
        jobOffers.stream()
            .map(job -> new Ranking<JobOfferView>(job, 0))
            .collect(Collectors.toList());

    for (Ranking<JobOfferView> ranking : rankings) {
      for (String property : propertiesList) {

        JobOfferView item = ranking.getItem();
        if (item.getName().toLowerCase().contains(property.toLowerCase()))
          ranking.rankPoints += 5;
        if (item.getLocation() != null && item.getLocation().toLowerCase().contains(property.toLowerCase()))
          ranking.rankPoints += 5;
        if (item.getEmployer().getName().toLowerCase().contains(property.toLowerCase()))
          ranking.rankPoints += 3;
        if (item.shortJobDescription != null && item.shortJobDescription.toLowerCase().contains(property.toLowerCase()))
          ranking.rankPoints += 2;
        for (Skill skill : item.getRequiredSkills()) {
          if (skill.getName()
              .toLowerCase()
              .contains(property.toLowerCase())) {
            ranking.rankPoints += 3;
          }
        }

        if (item
            .getRequiredSkills()
            .get(0)
            .getIndustry()
            .toString()
            .toLowerCase()
            .contains(property.toLowerCase())) {
          ranking.rankPoints += 1;
        }
      }
      System.out.println(ranking);
    }
    rankings.sort(Comparator.reverseOrder());

    System.out.println("======== Sorted ========");
    rankings.forEach(System.out::println);
    System.out.println(page);
    jobOffers = rankings.stream().map(Ranking::getItem).collect(Collectors.toList());
    int startIndex = page * PAGE_SIZE, endIndex = (page + 1) * PAGE_SIZE;
    if (startIndex > jobOffers.size()) return null;
    if (endIndex > jobOffers.size()) {
      endIndex = jobOffers.size();
    }
    System.out.println("startIndex = " + startIndex + ", endIndex = " + endIndex);
    Page<JobOfferView> pageToReturn =
        new PageImpl<JobOfferView>(
            jobOffers.subList(startIndex, endIndex),
            PageRequest.of(page, PAGE_SIZE),
            jobOffers.size());
    System.out.println("Page:" + pageToReturn.getContent());
    return pageToReturn;
  }

  @Override
  public Page<JobOfferView> customFindAllByRequiredSkillsPage(List<Long> skillIds, int page) {
    return jobOfferViewJpaRepository.customFindAllByRequiredSkills(
        skillIds, skillIds.size(), PageRequest.of(page, PAGE_SIZE));
  }

  @Override
  public Page<JobOfferView> viewJobOffersByUser(Long employerId, int page) {
    return jobOfferViewJpaRepository.getJobOffersByUserId(
        employerId, PageRequest.of(page, PAGE_SIZE));
  }

  @Override
  public List<JobOffer> getJobOffersDynamic(
      String name, String location, String empName, String desc, List<Long> skillIds) {

    JobOfferSpecification joSpec = new JobOfferSpecification();
    if (name != null && !name.isEmpty()) {
      joSpec.add(new SearchCriteria("name", name, SearchOperation.MATCH));
    }
    if (location != null && !location.isEmpty()) {
      joSpec.add(new SearchCriteria("location", name, SearchOperation.EQUAL));
    }
    //        if (empName != null && !empName.isEmpty()) {
    //            joSpec.add(new SearchCriteria("empName", name, SearchOperation.EQUAL));
    //        }
    if (desc != null && !desc.isEmpty()) {
      joSpec.add(new SearchCriteria("description", name, SearchOperation.EQUAL));
    }
    if (skillIds != null && !skillIds.isEmpty()) {
      joSpec.add(new SearchCriteria("skills", name, SearchOperation.IN));
    }
    return jobOfferJpaRepository.findAll(joSpec);
  }

  @Override
  public List<Long> testSearch() {

    List<Long> emps = new ArrayList<>();
    emps.add(1L);
    emps.add(2L);
    emps.add(3L);
    emps.add(4L);
    emps.add(5L);
    emps.add(6L);

    List<Long> returnedIds = jobOfferViewJpaRepository.testSearch(emps);

    System.out.println(returnedIds);
    return returnedIds;
  }
}
