package com.jobs.service.impl;

import com.jobs.model.Industry;
import com.jobs.model.JobOffer;
import com.jobs.model.Skill;
import com.jobs.model.User;
import com.jobs.persistence.JobOfferJpaRepository;
import com.jobs.persistence.SkillJpaRepository;
import com.jobs.persistence.UserJpaRepository;
import com.jobs.service.AdminSideService;
import java.util.Optional;
import org.springframework.stereotype.Service;

/** Created by HTomovski Date: 15-Jul-19 Time: 6:45 PM */
@Service
public class AdminSideServiceImpl implements AdminSideService {
  private final JobOfferJpaRepository jobOfferJpaRepository;
  private final SkillJpaRepository skillJpaRepository;
  private final UserJpaRepository userJpaRepository;

  public AdminSideServiceImpl(
      JobOfferJpaRepository jobOfferJpaRepository,
      SkillJpaRepository skillJpaRepository,
      UserJpaRepository userJpaRepository) {
    this.jobOfferJpaRepository = jobOfferJpaRepository;
    this.skillJpaRepository = skillJpaRepository;
    this.userJpaRepository = userJpaRepository;
  }

  @Override
  public Skill createNewSkill(String skillName, Industry industry) {
    Skill newSkill = new Skill();
    newSkill.setName(skillName);
    newSkill.setIndustry(industry);
    return skillJpaRepository.save(newSkill);
  }

  @Override
  public Skill updateSkill(Long skillId, String newSkillName) {
    Skill skill = null;
    Optional<Skill> skillOpt = skillJpaRepository.findById(skillId);
    if (skillOpt.isPresent()) {
      skill = skillOpt.get();
    }
    if (skill != null) skill.setName(newSkillName);
    return skillJpaRepository.save(skill);
  }

  @Override
  public Optional<Skill> viewSkill(String skillName) {
    return skillJpaRepository.findByName(skillName);
  }

  @Override
  public Optional<Skill> viewSkill(Long skillId) {
    return skillJpaRepository.findById(skillId);
  }

  @Override
  public void removeSkill(Long skillId) {
    skillJpaRepository.deleteById(skillId);
  }

  @Override
  public User createNewUser(User user) {
    return userJpaRepository.save(user);
  }

  @Override
  public User updateUser(User user) {
    return userJpaRepository.save(user);
  }

  @Override
  public Optional<User> viewUser(Long userId) {
    return userJpaRepository.findById(userId);
  }

  @Override
  public void removeUser(Long userId) {
    userJpaRepository.deleteById(userId);
  }

  @Override
  public JobOffer createNewJobOffer(JobOffer jobOffer) {
    return jobOfferJpaRepository.save(jobOffer);
  }

  @Override
  public JobOffer updateJobOffer(JobOffer jobOffer) {
    return jobOfferJpaRepository.save(jobOffer);
  }

  @Override
  public Optional<JobOffer> viewJobOffer(Long jobOfferId) {
    return jobOfferJpaRepository.findById(jobOfferId);
  }

  @Override
  public void removeJobOffer(Long jobOfferId) {
    jobOfferJpaRepository.deleteById(jobOfferId);
  }
}
