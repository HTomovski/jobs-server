package com.jobs.service.impl;

import com.jobs.config.security.JwtService;
import com.jobs.config.security.UserPrincipal;
import com.jobs.model.Token;
import com.jobs.model.User;
import com.jobs.model.enums.TokenType;
import com.jobs.model.exceptions.NoSuchUserException;
import com.jobs.model.payloads.requests.LoginRequest;
import com.jobs.model.payloads.responses.JwtAuthenticationResponse;
import com.jobs.persistence.TokenJpaRepository;
import com.jobs.persistence.UserJpaRepository;
import com.jobs.service.AuthenticationService;
import java.util.Date;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;
  private final UserJpaRepository userRepository;
  private final TokenJpaRepository tokenRepository;

  @Value("${application.security.jwt.expiration}")
  private long JWT_EXPIRATION_MS;

  @Override
  public JwtAuthenticationResponse authenticate(LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            loginRequest.getEmail(),
            loginRequest.getPassword()));

    Optional<User> userOptional = userRepository.findByEmail(loginRequest.getEmail());

    User user;

    if(userOptional.isEmpty())
      throw new NoSuchUserException();

    user = userOptional.get();

    UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
    String jwt = jwtService.generateToken(userPrincipal);

    revokeAllUserTokens(user);
    saveUserToken(user, jwt);

//    SecurityContextHolder.getContext().setAuthentication(authentication);

//    Map<String, Object> provider = new HashMap<>();
//    provider.put("provider", "SYSTEM");

    return JwtAuthenticationResponse
        .builder()
        .accessToken(jwt)
        .expiresAt(new Date(System.currentTimeMillis() + JWT_EXPIRATION_MS).getTime())
        .build();
  }

  private void saveUserToken(User user, String jwtToken) {
    var token = Token.builder()
        .user(user)
        .token(jwtToken)
        .tokenType(TokenType.BEARER)
        .expired(false)
        .revoked(false)
        .build();
    tokenRepository.save(token);
  }

  private void revokeAllUserTokens(User user) {
    var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
    if (validUserTokens.isEmpty())
      return;
    validUserTokens.forEach(token -> {
      token.setExpired(true);
      token.setRevoked(true);
    });
    tokenRepository.saveAll(validUserTokens);
  }

  private void deleteAllTokensForUser(User user) {
    tokenRepository.deleteAll();
  }
}
