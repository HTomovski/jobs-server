package com.jobs.service.impl;

import com.jobs.model.Candidate;
import java.util.Objects;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class TransactionalTestService {

  private String str;

  @Transactional
  public Candidate fetchCandidate() {
    return null;
  }

  @Transactional
  public Candidate fetchCandidate2() {
    return null;
  }

  public TransactionalTestService() {}

  public String getStr() {
    return str;
  }

  public void setStr(String str) {
    this.str = str;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TransactionalTestService that = (TransactionalTestService) o;
    return Objects.equals(str, that.str);
  }

  @Override
  public int hashCode() {
    return Objects.hash(str);
  }

  @Override
  public String toString() {
    return "TransactionalTestService{" + "str='" + str + '\'' + '}';
  }
}
