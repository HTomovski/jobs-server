package com.jobs.service.impl;

import com.jobs.model.Industry;
import com.jobs.model.Skill;
import com.jobs.model.exceptions.NoSuchIndustryException;
import com.jobs.model.exceptions.SkillAlreadyExistsException;
import com.jobs.persistence.IndustryJpaRepository;
import com.jobs.persistence.SkillJpaRepository;
import com.jobs.service.SkillService;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/** Created by HTomovski Date: 09-Sep-19 Time: 9:41 AM */
@Service
public class SkillServiceImpl implements SkillService {

  private final SkillJpaRepository skillRepository;
  private final IndustryJpaRepository industryRepository;

  public SkillServiceImpl(
      SkillJpaRepository skillRepository, IndustryJpaRepository industryRepository) {
    this.skillRepository = skillRepository;
    this.industryRepository = industryRepository;
  }

  @Override
  public List<Skill> getSkillsByIndustryId(Long industryId) {
    return skillRepository.findAllByIndustry_Id(industryId);
  }

  @Override
  public Skill createSkill(String skillName, Long industryId) {
    if (!skillRepository.findByName(skillName).isPresent()) {
      Skill skill = new Skill();
      skill.setName(skillName);
      Optional<Industry> industry = industryRepository.findById(industryId);
      if (industry.isPresent()) {
        skill.setIndustry(industry.get());
        return skillRepository.save(skill);
      }
      throw new NoSuchIndustryException();
    }
    throw new SkillAlreadyExistsException();
  }
}
