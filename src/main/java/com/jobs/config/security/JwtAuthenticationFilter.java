package com.jobs.config.security;

import com.jobs.persistence.TokenJpaRepository;
import com.jobs.service.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

  private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

  private final JwtService jwtService;

  private final UserService userDetailsService;

  private final TokenJpaRepository tokenRepository;

//  @Override
//  protected void doFilterInternal(
//      @NonNull HttpServletRequest request,
//      @NonNull HttpServletResponse response,
//      @NonNull FilterChain filterChain)
//      throws ServletException, IOException {
//    try {
//      String jwt = getJwtFromRequest(request);
//      if (StringUtils.hasText(jwt) && jwtHelper.validateToken(jwt)) {
//
//        String provider = jwtHelper.getClaims(jwt).get("provider", String.class);
//        if (provider.equals("SYSTEM")) {
//
//          Long userId = jwtHelper.getUserIdFromJWT(jwt);
//          UserDetails userDetails = userAccessService.findUserById(userId);
//          userDetails.getAuthorities();
//          UsernamePasswordAuthenticationToken authentication =
//              new UsernamePasswordAuthenticationToken(
//                  userDetails, null, userDetails.getAuthorities());
//          authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//          SecurityContextHolder.getContext().setAuthentication(authentication);
//        } else {
//
//          Claims claims = jwtHelper.getClaims(jwt);
//          List<SimpleGrantedAuthority> grantedAuthorities =
//              Stream.of(new SimpleGrantedAuthority("ROLE_USER")).collect(Collectors.toList());
//          String picture = "";
//          String email = claims.get("email", String.class);
//          if (claims.get("provider", String.class).equals("FACEBOOK")) {
//
//            picture =
//                "http://graph.facebook.com/"
//                    + claims.get("id", String.class)
//                    + "/picture?type=large";
//          } else if (claims.get("provider", String.class).equals("GOOGLE")) {
//
//            picture = claims.get("picture", String.class);
//          } else if (claims.get("provider", String.class).equals("GITHUB")) {
//
//            picture = claims.get("avatar_url", String.class);
//            email = claims.get("blog", String.class);
//          }
//
//          UserPrincipal principal =
//              new UserPrincipal(
//                  0L,
//                  claims.get("name", String.class),
//                  email,
//                  "",
//                  claims.get("provider", String.class),
//                  picture,
//                  grantedAuthorities);
//          SecurityContextHolder.getContext()
//              .setAuthentication(
//                  new PreAuthenticatedAuthenticationToken(principal, null, grantedAuthorities));
//        }
//      }
//    } catch (Exception ex) {
//      logger.error("Could not set user authentication in security context", ex);
//    }
//
//    filterChain.doFilter(request, response);
//  }

  @Override
  protected void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain filterChain
  ) throws ServletException, IOException {

    String path = request.getServletPath();

    if (path.contains("/users/register")
    || path.contains("/users/login")) {

      filterChain.doFilter(request, response);
      return;
    }
    final String authHeader = request.getHeader("Authorization");
    final String jwt;
    final String userEmail;
    if (authHeader == null || !authHeader.startsWith("Bearer ")) {
      filterChain.doFilter(request, response);
      return;
    }
    jwt = authHeader.substring(7);
    userEmail = jwtService.extractUsername(jwt);
    if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
      UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
      var isTokenValid = tokenRepository.findByToken(jwt)
          .map(t -> !t.isExpired() && !t.isRevoked())
          .orElse(false);
      if (jwtService.isTokenValid(jwt, userDetails) && isTokenValid) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
            userDetails,
            null,
            userDetails.getAuthorities()
        );
        authToken.setDetails(
            new WebAuthenticationDetailsSource().buildDetails(request)
        );
        SecurityContextHolder.getContext().setAuthentication(authToken);
      }
    }
    filterChain.doFilter(request, response);
  }
}
