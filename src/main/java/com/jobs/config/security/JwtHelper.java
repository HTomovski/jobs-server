//package com.jobs.config.security;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.ExpiredJwtException;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.MalformedJwtException;
//import io.jsonwebtoken.SignatureAlgorithm;
//import io.jsonwebtoken.UnsupportedJwtException;
//import io.jsonwebtoken.security.Keys;
//import java.nio.charset.StandardCharsets;
//import java.security.Key;
//import java.util.Date;
//import java.util.Map;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.Authentication;
//import org.springframework.stereotype.Component;
//
//@Component
//public class JwtHelper {
//  private static final Logger logger = LoggerFactory.getLogger(JwtHelper.class);
//
//  @Value("${application.security.jwt.secret-key}")
//  private String jwtSecret;
//
//  @Value("${application.security.jwt.expiration}")
//  private int jwtExpirationInMs;
//
//  public String generateToken(Authentication authentication) {
//
//    UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
//    Claims claims = Jwts.claims().setSubject(Long.toString(userPrincipal.getId()));
//    claims.put("provider", "SYSTEM");
//
//    Date now = new Date();
//    Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);
//
//    return Jwts.builder()
//        .setClaims(claims)
//        .setIssuedAt(new Date())
//        .setExpiration(expiryDate)
//        .signWith(getSigningKey(), SignatureAlgorithm.HS512)
//        .compact();
//  }
//
//  private Key getSigningKey() {
//    byte[] keyBytes = this.jwtSecret.getBytes(StandardCharsets.UTF_8);
//    return Keys.hmacShaKeyFor(keyBytes);
//  }
//
//  public String generateToken(String providerId, String provider, Map authDetailsMap) {
//    Claims claims = Jwts.claims().setSubject(providerId);
//    claims.putAll(authDetailsMap);
//    claims.put("provider", provider);
//
//    Date validity = new Date(new Date().getTime() + this.jwtExpirationInMs);
//    return Jwts.builder()
//        .setClaims(claims)
//        .setIssuedAt(new Date())
//        .setExpiration(validity)
//        .signWith(SignatureAlgorithm.HS256, this.jwtSecret)
//        .compact();
//  }
//
//  public Long getUserIdFromJWT(String token) {
//    Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
//
//    if (claims.getSubject().length() >= 20) {
//      return null;
//    }
//    return Long.parseLong(claims.getSubject());
//  }
//
//  public Claims getClaims(String token) {
//    return (Claims) Jwts
//        .parser()
//        .setSigningKey(this.jwtSecret)
//        .parseClaimsJws(token)
//        .getBody();
//  }
//
//  public boolean validateToken(String authToken) {
//    try {
//      // TODO ovde pagja
//      Jwts.parserBuilder()
//          .setSigningKey(getSigningKey())
//          .build()
//          .parseClaimsJws(authToken);
////      Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
//      return true;
//    } catch (SecurityException ex) {
//      logger.error("Invalid JWT signature");
//    } catch (MalformedJwtException ex) {
//      logger.error("Invalid JWT token");
//    } catch (ExpiredJwtException ex) {
//      logger.error("Expired JWT token");
//    } catch (UnsupportedJwtException ex) {
//      logger.error("Unsupported JWT token");
//    } catch (IllegalArgumentException ex) {
//      logger.error("JWT claims string is empty.");
//    }
//    return false;
//  }
//}
