//package com.jobs.config.security.oauth;
//
//import com.jobs.config.security.JwtHelper;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.oauth2.client.OAuth2ClientContext;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//
//@Configuration
//@OAuthProvider
//public class FacebookAuthentication extends AbstractOAuthConfigurer {
//
//  public FacebookAuthentication(
//      @Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext,
//      JwtHelper jwtHelper) {
//    super(oauth2ClientContext, jwtHelper);
//  }
//
//  @Bean(name = "facebookSuccessHandler")
//  @Override
//  protected AuthenticationSuccessHandler successHandler() {
//    return new SignInSuccessHandler("FACEBOOK", this.jwtHelper);
//  }
//
//  @Bean(name = "facebookFailureHandler")
//  @Override
//  protected AuthenticationFailureHandler failureHandler() {
//    return new SignInFailureHandler();
//  }
//
//  @Bean(name = "facebookConfig")
//  @ConfigurationProperties("facebook")
//  public ClientResources clientConfig() {
//    return new ClientResources();
//  }
//
//  @Override
//  protected String entryPointUrl() {
//    return "/login/facebook";
//  }
//}
