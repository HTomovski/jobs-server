//package com.jobs.config.security.oauth;
//
//import java.io.IOException;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//
//public class SignInFailureHandler implements AuthenticationFailureHandler {
//
//  @Value("${app.client_url}")
//  public String clientAppUrl;
//
//  @Override
//  public void onAuthenticationFailure(
//      HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
//      throws IOException, ServletException {
//    try {
//      System.out.println("In failure! Params: ");
//      System.out.println(request.getParameterMap());
//    } finally {
//      response.sendRedirect(clientAppUrl + "/login.html?status=oauth-failure");
//    }
//  }
//}
