package com.jobs.config.security;

import com.jobs.config.security.auditing.ApplicationAuditAware;
import com.jobs.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
//@EnableOAuth2Client
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig {

  private static final String[] WHITELIST_URLS = {
      "/",
      "/favicon.ico",
      "/**.png",
      "/**.gif",
      "/**.svg",
      "/**.jpg",
      "/**.html",
      "/**.css",
      "/**.js"
  };
  // todo: Upgrade to the new alternatives from the deprecated OAuth2 stuff.
//  @Qualifier("oauth2ClientContext")
//  @Autowired
//  OAuth2ClientContext oauth2ClientContext;
  private final ApplicationContext applicationContext;
  private final UserService userAccessService;
//  private final JwtHelper jwtHelper;
  private final JwtService jwtService;
  private final JwtAuthenticationFilter jwtAuthenticationFilter;
  @Value("${app.client_url}")
  private String client_url;
  @Autowired
  private JwtAuthenticationEntryPoint unauthorizedHandler;

//  @Bean
//  public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
//    InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//    manager.createUser(User.withUsername("user")
//        .password(passwordEncoder.encode("123123"))
//        .roles("USER")
//        .build());
//    manager.createUser(User.withUsername("admin")
//        .password(passwordEncoder.encode("123123"))
//        .roles("USER", "ADMIN")
//        .build());
//    return manager;
//  }

//  @Bean
//  public JwtAuthenticationFilter jwtAuthenticationFilter() {
//    return new JwtAuthenticationFilter(jwtService, userAccessService);
//  }

  @Bean
  @Order(0)
  public RequestContextListener requestContextListener() {
    return new RequestContextListener();
  }

//  //@Bean
//  public AuthenticationManager authenticationManager(
//      AuthenticationConfiguration authenticationConfiguration) throws Exception {
//    return authenticationConfiguration.getAuthenticationManager();
//  }
//
//  @Bean
//  public AuthenticationManager authenticationManager(UserDetailsService userDetailsService) {
//    var authProvider = new DaoAuthenticationProvider();
//    authProvider.setPasswordEncoder(passwordEncoder());
//    authProvider.setUserDetailsService(userDetailsService);
//    return new ProviderManager(authProvider);
//  }

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

    //.cors(AbstractHttpConfigurer::disable)
    return http
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(auth -> auth
            .requestMatchers(
                WHITELIST_URLS).permitAll()
            .requestMatchers("/users/**").permitAll()
            .requestMatchers("/ws", "/ws/*", "/topic").permitAll()
            .anyRequest().permitAll())
        .sessionManagement(conf -> conf.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .authenticationProvider(authenticationProvider())
        .addFilterBefore(
            jwtAuthenticationFilter,
            UsernamePasswordAuthenticationFilter.class
        )
        .logout(conf -> {
          conf.clearAuthentication(true);
          conf.invalidateHttpSession(true);
          conf.logoutSuccessUrl(client_url + "/login.html");
          conf.permitAll();
        })
        //.exceptionHandling(conf -> conf.authenticationEntryPoint(unauthorizedHandler))
        //.oauth2Client()
        .build();
  }

  @Bean
  public AuthenticationProvider authenticationProvider() {

    DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setPasswordEncoder(passwordEncoder());
    authenticationProvider.setUserDetailsService(userAccessService);
    return authenticationProvider;
  }

  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration config)
      throws Exception {

    return config.getAuthenticationManager();
  }

//    http.cors(AbstractHttpConfigurer::disable);
//    return http.csrf(AbstractHttpConfigurer::disable)
//        .authorizeHttpRequests(auth -> auth
//        .requestMatchers("/token/**").permitAll()
//        .anyRequest().authenticated()
//    )
//        .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//        .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
//        .httpBasic(Customizer.withDefaults())
//        .build();
//  }
//  @Override
//  public void configure(AuthenticationManagerBuilder authenticationManagerBuilder)
//      throws Exception {
//    super.configure(authenticationManagerBuilder);
//
//    // We set the service for checking username & password here
//    authenticationManagerBuilder
//        .userDetailsService(userAccessService)
//        .passwordEncoder(passwordEncoder());
//  }
//
//  protected void configure(HttpSecurity http) throws Exception {
//    http.cors(AbstractHttpConfigurer::disable)
//        .csrf(AbstractHttpConfigurer::disable)
//        .exceptionHandling(conf -> conf.authenticationEntryPoint(unauthorizedHandler))
//        .sessionManagement(conf -> conf.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//        .logout(conf -> {
//          conf.clearAuthentication(true);
//          conf.invalidateHttpSession(true);
//          conf.logoutSuccessUrl(client_url + "/login.html");
//          conf.permitAll();
//        })
//        .authorizeHttpRequests(auth -> {
//          auth.requestMatchers(
//                  "/",
//                  "/favicon.ico",
//                  "/**/*.png",
//                  "/**/*.gif",
//                  "/**/*.svg",
//                  "/**/*.jpg",
//                  "/**/*.html",
//                  "/**/*.css",
//                  "/**/*.js").permitAll()
//              .anyRequest();
//          auth.requestMatchers("/users/**").permitAll();
//          auth.requestMatchers("/ws", "/ws/*", "/topic").permitAll();
//          auth.anyRequest().permitAll();
//        });
////
////    http.cors()
////        .and()
////        .csrf()
////        .disable()
////        .exceptionHandling()
////        .authenticationEntryPoint(unauthorizedHandler)
////        .and()
////        .sessionManagement()
////        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
////        .and()
////        .logout()
////        .clearAuthentication(true)
////        .invalidateHttpSession(true)
////        .logoutSuccessUrl(client_url + "/login.html")
////        .permitAll()
////        .and()
////        .authorizeHttpRequests()
////        .requestMatchers(
////            "/",
////            "/favicon.ico",
////            "/**/*.png",
////            "/**/*.gif",
////            "/**/*.svg",
////            "/**/*.jpg",
////            "/**/*.html",
////            "/**/*.css",
////            "/**/*.js")
////        .permitAll()
////        .requestMatchers("/users/**")
////        .permitAll()
////        .requestMatchers("/ws", "/ws/*", "/topic")
////        .permitAll()
////        .anyRequest()
////        .permitAll();
//
//    // Add our custom JWT security filter
//    List<SecurityConfigurerAdapter> additionalOAuthProviders = fetchAdditionalOAuthConfigurations();
//    for (SecurityConfigurerAdapter provider : additionalOAuthProviders) {
//      http.apply(provider);
//    }
//    http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
//  }

  @Bean
  public static PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuditorAware<Integer> auditorAware() {
    return new ApplicationAuditAware();
  }

//  @Bean
//  public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
//    FilterRegistrationBean registration = new FilterRegistrationBean();
//    registration.setFilter(filter);
//    registration.setOrder(-100);
//    return registration;
//  }

//  @Bean
//  public FilterRegistrationBean oauth2ClientFilterRegistration(Filter filter) {
//    FilterRegistrationBean registration = new FilterRegistrationBean();
//    registration.setFilter(filter);
//    registration.setOrder(-100);
//    return registration;
//  }
//
//
//  private List<SecurityConfigurerAdapter> fetchAdditionalOAuthConfigurations() {
//    return applicationContext.getBeansWithAnnotation(OAuthProvider.class).values().stream()
//        .map(i -> (SecurityConfigurerAdapter) i)
//        .collect(Collectors.toList());
//  }

}
