package com.jobs.persistence;

import com.jobs.model.JobOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface JobOfferJpaRepository extends JpaRepository<JobOffer, Long>, JpaSpecificationExecutor<JobOffer> {

}
