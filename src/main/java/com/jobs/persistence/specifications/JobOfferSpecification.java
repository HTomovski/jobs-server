package com.jobs.persistence.specifications;

import com.jobs.model.JobOffer;
import com.jobs.persistence.specifications.criteria.SearchCriteria;
import com.jobs.persistence.specifications.operations.SearchOperation;
import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class JobOfferSpecification implements Specification<JobOffer> {

  private final List<SearchCriteria> searchCriteriaList = new ArrayList<>();

  public void add(SearchCriteria criteria) {
    searchCriteriaList.add(criteria);
  }

  @Override
  public Predicate toPredicate(
      Root<JobOffer> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    // create a new predicate list
    List<Predicate> predicates = new ArrayList<>();

    // add criteria to predicate
    for (SearchCriteria criteria : searchCriteriaList) {
      if (criteria.getSearchOperation().equals(SearchOperation.GREATER_THAN)) {
        predicates.add(
            criteriaBuilder.greaterThan(root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.LESS_THAN)) {
        predicates.add(
            criteriaBuilder.lessThan(root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
        predicates.add(
            criteriaBuilder.greaterThanOrEqualTo(
                root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
        predicates.add(
            criteriaBuilder.lessThanOrEqualTo(root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.NOT_EQUAL)) {
        predicates.add(
            criteriaBuilder.notEqual(root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.EQUAL)) {
        predicates.add(criteriaBuilder.equal(root.get(criteria.getField()), criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.MATCH)) {
        predicates.add(
            criteriaBuilder.like(
                criteriaBuilder.lower(root.get(criteria.getField())),
                "%" + criteria.getValue().toLowerCase() + "%"));
      } else if (criteria.getSearchOperation().equals(SearchOperation.MATCH_END)) {
        predicates.add(
            criteriaBuilder.like(
                criteriaBuilder.lower(root.get(criteria.getField())),
                criteria.getValue().toLowerCase() + "%"));
      } else if (criteria.getSearchOperation().equals(SearchOperation.MATCH_START)) {
        predicates.add(
            criteriaBuilder.like(
                criteriaBuilder.lower(root.get(criteria.getField())),
                "%" + criteria.getValue().toLowerCase()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.IN)) {
        predicates.add(
            criteriaBuilder.in(root.get(criteria.getField())).value(criteria.getValue()));
      } else if (criteria.getSearchOperation().equals(SearchOperation.NOT_IN)) {
        predicates.add(criteriaBuilder.not(root.get(criteria.getField())).in(criteria.getValue()));
      }
    }

    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
  }
}
