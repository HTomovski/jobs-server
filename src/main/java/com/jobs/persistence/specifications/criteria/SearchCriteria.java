package com.jobs.persistence.specifications.criteria;

import com.jobs.persistence.specifications.operations.SearchOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteria {

  private String field;
  private String value;
  private SearchOperation searchOperation;
}
