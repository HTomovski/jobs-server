package com.jobs.persistence;

import com.jobs.model.views.UserViewShort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserViewJpaRepository extends JpaRepository<UserViewShort, Long> {

  UserViewShort findByNameContaining(String name);
}
