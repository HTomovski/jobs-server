package com.jobs.persistence;

import com.jobs.model.Industry;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by HTomovski
 * Date: 09-Sep-19
 * Time: 1:47 PM
 */
public interface IndustryJpaRepository extends JpaRepository<Industry, Long> {

}
