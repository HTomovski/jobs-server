package com.jobs.persistence;

import com.jobs.model.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/** Created by HTomovski Date: 22-Aug-19 Time: 3:36 PM */
public interface UserJpaRepository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email);
}
