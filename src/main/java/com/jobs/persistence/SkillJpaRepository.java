package com.jobs.persistence;

import com.jobs.model.Skill;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillJpaRepository extends JpaRepository<Skill, Long> {

    Optional<Skill> findByName(String skillName);

    List<Skill> findAllByIndustry_Id(Long industryId);
}
