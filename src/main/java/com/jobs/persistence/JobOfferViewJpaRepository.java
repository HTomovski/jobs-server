package com.jobs.persistence;

import com.jobs.model.views.JobOfferView;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobOfferViewJpaRepository extends JpaRepository<JobOfferView, Long> {

  @Query(
      value =
          "select * from job_offer j  "
              + "where :skills_num =  "
              + "   ( select count(*) from job_required_skills jrs "
              + "   where j.id=jrs.job_offer_id and jrs.required_skill_id in :req_skills )",
      nativeQuery = true)
  Page<JobOfferView> customFindAllByRequiredSkills(
      @Param("req_skills") List<Long> skills,
      @Param("skills_num") int skillsNum,
      Pageable pageable);

  @Query(
      value =
          "select j.id, s.id, i.id, j.emp_id, j.location, j.name, j.short_desc, emp.name, s.name, i.name "
              + "from (job_offer j join app_user emp on j.emp_id = emp.id) "
              + "   join job_required_skills jrs on j.id = jrs.job_offer_id join skill s on s.id = jrs.required_skill_id "
              + "   join industry i on s.industry_id = i.id "
              + "where j.name like CONCAT('%',:property,'%')"
              + "   or j.location like CONCAT('%',:property,'%')"
              + "   or emp.name like CONCAT('%',:property,'%')"
              + "   or j.short_desc like CONCAT('%',:property,'%')"
              + "   or s.name like CONCAT('%',:property,'%')"
              + "   or i.name like CONCAT('%',:property,'%')"
              + "",
      nativeQuery = true)
  List<JobOfferView> customFindAllBySingleUnknownField(@Param("property") String property);

  @Query(value = "select * " + "from job_offer j " + "where j.emp_id = :emp_id", nativeQuery = true)
  Page<JobOfferView> getJobOffersByUserId(@Param("emp_id") Long employerId, Pageable pageable);

  JobOfferView getById(Long jobId);

  @Query(
      value = "select j.id"
          + " from job_offer j"
          + " where j.name is not null"
          + " and j.emp_id in :employers",
      nativeQuery = true
  )
  List<Long> testSearch(List<Long> employers);
}
