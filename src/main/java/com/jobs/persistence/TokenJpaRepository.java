package com.jobs.persistence;

import com.jobs.model.Token;
import com.jobs.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TokenJpaRepository extends JpaRepository<Token, Integer> {

  @Query(value = """
      select t from Token t inner join User u\s
      on t.user.id = u.id\s
      where u.id = :id and (t.expired = false or t.revoked = false)\s
      """)
  List<Token> findAllValidTokenByUser(Long id);

  Optional<Token> findByToken(String token);

  @Modifying
  @Query(value = """
      DELETE
      FROM Token tok
      WHERE tok.user = :user
      """)
  void deleteAllByUser(@Param(value = "user") User user);
}