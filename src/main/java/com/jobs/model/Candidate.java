package com.jobs.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "candidate")
public class Candidate extends User {

  private String CV;

  @Column(name = "past_employments")
  private String pastEmployments;

  @ManyToMany
  @JoinTable(
      name = "candidate_skill",
      joinColumns = @JoinColumn(name = "candidate_id"),
      inverseJoinColumns = @JoinColumn(name = "skill_id"))
  private List<Skill> skills = new ArrayList<>();

  public Candidate() {}

  public Candidate(User user) {
    this.id = user.getId();
    this.name = user.getName();
    this.email = user.getEmail();
    this.password = user.getPassword();
    this.role = user.getRole();
  }

  public String getCV() {
    return CV;
  }

  public void setCV(String CV) {
    this.CV = CV;
  }

  public String getPastEmployments() {
    return pastEmployments;
  }

  public void setPastEmployments(String pastEmployments) {
    this.pastEmployments = pastEmployments;
  }

  public List<Skill> getSkills() {

    return Collections.unmodifiableList(skills);
  }

  public void addSkill(Skill skill) {
    this.skills.add(skill);
  }

  @Override
  public String toString() {
    return "Candidate{"
        + "CV='" + CV + '\''
        + ", pastEmployments='" + pastEmployments + '\''
        + ", skills=" + skills
        + ", id=" + id
        + ", name='" + name + '\''
        + ", shortDescription='" + shortDescription + '\''
        + ", homeTown='" + homeTown + '\''
        + ", email='" + email + '\''
        + ", password='" + password + '\''
        + ", role=" + role
        + '}';
  }
}
