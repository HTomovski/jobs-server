package com.jobs.model.views;

import com.jobs.model.Skill;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.springframework.data.annotation.Immutable;

@Entity
@Immutable
@Table(name = "job_offer")
public class JobOfferView {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String location;

  // @Column(name = "emp_id")
  // od konsultaciite - preporakata bese da se koristi samo id za employerot, ama jas si napraviv
  // view so id i name.
  @ManyToOne
  @JoinColumn(name = "emp_id")
  private UserViewShort employer;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "job_required_skills",
      joinColumns = @JoinColumn(name = "job_offer_id"),
      inverseJoinColumns = @JoinColumn(name = "required_skill_id"))
  private List<Skill> requiredSkills = new ArrayList<>();

  @Column(name = "short_desc")
  public String shortJobDescription;

  public JobOfferView() {}

  public JobOfferView(
      String name,
      String location,
      UserViewShort employer,
      List<Skill> requiredSkills,
      String shortJobDescription) {
    this.name = name;
    this.location = location;
    this.employer = employer;
    this.requiredSkills = requiredSkills;
    this.shortJobDescription = shortJobDescription;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public UserViewShort getEmployer() {
    return employer;
  }

  public void setEmployer(UserViewShort employerId) {
    this.employer = employerId;
  }

  public List<Skill> getRequiredSkills() {
    return Collections.unmodifiableList(requiredSkills);
  }

  public void addRequiredSkill(Skill requiredSkill) {
    this.requiredSkills.add(requiredSkill);
  }

  public void addRequiredSkills(List<Skill> requiredSkills) {
    this.requiredSkills.addAll(requiredSkills);
  }

  public String getShortJobDescription() {
    return shortJobDescription;
  }

  public void setShortJobDescription(String shortJobDescription) {
    this.shortJobDescription = shortJobDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    JobOfferView jobOffer = (JobOfferView) o;
    return Objects.equals(id, jobOffer.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "JobOfferView{"
        + "id=" + id
        + ", name='" + name + '\''
        + ", location='" + location + '\''
        + ", employer=" + employer
        + ", requiredSkills=" + requiredSkills
        + ", shortJobDescription='" + shortJobDescription + '\''
        + '}';
  }
}
