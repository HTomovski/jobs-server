package com.jobs.model.views;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Objects;
import org.springframework.data.annotation.Immutable;

@Entity
@Immutable
@Table(name = "app_user")
public class UserViewShort {

  @Id
  private Long id;

  private String name;

  public UserViewShort() {}

  public UserViewShort(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserViewShort that = (UserViewShort) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "UserViewShort{"
        + "id=" + id
        + ", name='" + name + '\''
        + '}';
  }
}
