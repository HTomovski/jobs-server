package com.jobs.model.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class AdvancedJobSearchParams {
  private String name;
  private String location;
  private String empName;
  private String description;
  private List<Long> skillIds = new ArrayList<>();

  public AdvancedJobSearchParams() {}

  public AdvancedJobSearchParams(
      String name, String location, String empName, String description, List<Long> skillIds) {
    this.name = name;
    location = location;
    this.empName = empName;
    this.description = description;
    if (skillIds != null) {
      this.skillIds.addAll(skillIds);
    }
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    location = location;
  }

  public String getEmpName() {
    return empName;
  }

  public void setEmpName(String empName) {
    this.empName = empName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Long> getSkillIds() {
    return skillIds;
  }

  public void setSkillIds(List<Long> skillIds) {
    this.skillIds = skillIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AdvancedJobSearchParams that = (AdvancedJobSearchParams) o;
    // boolean skillsAreEqual = true;

    if (skillIds.size() != that.skillIds.size()) return false;
    skillIds.sort(Comparator.naturalOrder());
    that.skillIds.sort(Comparator.naturalOrder());
    for (int i = 0; i < skillIds.size(); i++) {
      if (!skillIds.get(i).equals(that.skillIds.get(i))) return false;
    }

    return Objects.equals(name, that.name)
        && Objects.equals(location, that.location)
        && Objects.equals(empName, that.empName)
        && Objects.equals(description, that.description)
        && Objects.equals(skillIds, that.skillIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, location, empName, description, skillIds);
  }

  @Override
  public String toString() {
    return "AdvancedJobSearchParams{"
        + "name='" + name + '\''
        + ", Location='" + location + '\''
        + ", empName='" + empName + '\''
        + ", description='" + description + '\''
        + ", skillIds=" + skillIds
        + '}';
  }
}
