package com.jobs.model.utils;

public class Ranking<T> implements Comparable<Ranking<T>> {

  private T item;

  public int rankPoints;

  public Ranking() {}

  public Ranking(T item, int rankPoints) {
    this.item = item;
    this.rankPoints = rankPoints;
  }

  public T getItem() {
    return item;
  }

  public void setItem(T item) {
    this.item = item;
  }

  public int getRankPoints() {
    return rankPoints;
  }

  public void setRankPoints(int rankPoints) {
    this.rankPoints = rankPoints;
  }

  @Override
  public int compareTo(Ranking o) {
    return Integer.compare(this.rankPoints, o.rankPoints);
  }

  @Override
  public String toString() {
    return "Ranking{"
        + "rankPoints = " + rankPoints
        + ", item = " + item
        + '}';
  }
}
