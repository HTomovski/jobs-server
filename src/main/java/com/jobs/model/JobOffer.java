package com.jobs.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import java.util.List;
import java.util.Objects;

// @NamedEntityGraph(
//        name = "full-job-offer-graph",
//        attributeNodes = {
//                @NamedAttributeNode("employer"),
//                @NamedAttributeNode("requiredSkills"),
//        }
// )

@Entity
@Table(name = "job_offer")
public class JobOffer {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String location;

  @ManyToOne
  @JoinColumn(name = "emp_id")
  private Employer employer;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "job_required_skills",
      joinColumns = @JoinColumn(name = "job_offer_id"),
      inverseJoinColumns = @JoinColumn(name = "required_skill_id"))
  private List<Skill> requiredSkills;

  @Column(name = "short_desc")
  private String shortJobDescription;

  public JobOffer() {}

  public JobOffer(
      String name,
      String location,
      Employer employer,
      List<Skill> requiredSkills,
      String shortJobDescription) {
    this.name = name;
    this.location = location;
    this.employer = employer;
    this.requiredSkills = requiredSkills;
    this.shortJobDescription = shortJobDescription;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Employer getEmployer() {
    return employer;
  }

  public void setEmployer(Employer employer) {
    this.employer = employer;
  }

  public List<Skill> getRequiredSkills() {
    return requiredSkills;
  }

  public void setRequiredSkills(List<Skill> requiredSkills) {
    this.requiredSkills = requiredSkills;
  }

  public String getShortJobDescription() {
    return shortJobDescription;
  }

  public void setShortJobDescription(String shortJobDescription) {
    this.shortJobDescription = shortJobDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    JobOffer jobOffer = (JobOffer) o;
    return Objects.equals(id, jobOffer.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "JobOffer{"
        + "id=" + id
        + ", name='" + name + '\''
        + ", location='" + location + '\''
        + ", employer=" + employer
        + ", requiredSkills=" + requiredSkills
        + ", shortJobDescription='" + shortJobDescription + '\''
        + '}';
  }
}
