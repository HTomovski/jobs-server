package com.jobs.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "employer")
public class Employer extends User {

  private String companyInfo;

  public Employer() {}

  public Employer(User user) {
    this.id = user.getId();
    this.name = user.getName();
    this.email = user.getEmail();
    this.password = user.getPassword();
    this.role = user.getRole();
  }

  public String getCompanyInfo() {
    return companyInfo;
  }

  public void setCompanyInfo(String companyInfo) {
    this.companyInfo = companyInfo;
  }

  @Override public String toString() {
    return "Employer{" +
    "companyInfo='" + companyInfo + '\'' +
    ", id=" + id +
    ", name='" + name + '\'' +
    ", shortDescription='" + shortDescription + '\'' +
    ", homeTown='" + homeTown + '\'' +
    ", email='" + email + '\'' +
    ", password='" + password + '\'' +
    ", role=" + role +
    '}';
}}
