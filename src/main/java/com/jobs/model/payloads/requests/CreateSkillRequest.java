package com.jobs.model.payloads.requests;

import java.util.Objects;
import org.antlr.v4.runtime.misc.NotNull;

public class CreateSkillRequest {

  @NotNull
  public Long industryId;

  @NotNull
  public String skillName;

  public CreateSkillRequest() {}

  public CreateSkillRequest(Long industryId, String skillName) {
    this.industryId = industryId;
    this.skillName = skillName;
  }

  public Long getIndustryId() {
    return industryId;
  }

  public void setIndustryId(Long industryId) {
    this.industryId = industryId;
  }

  public String getSkillName() {
    return skillName;
  }

  public void setSkillName(String skillName) {
    this.skillName = skillName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CreateSkillRequest that = (CreateSkillRequest) o;
    return industryId.equals(that.industryId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(industryId);
  }

  @Override
  public String toString() {
    return "CreateSkillRequest{"
        + "industryId=" + industryId
        + ", skillName='" + skillName + '\''
        + '}';
  }
}
