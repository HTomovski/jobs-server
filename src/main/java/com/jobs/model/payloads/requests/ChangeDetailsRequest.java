package com.jobs.model.payloads.requests;

import org.antlr.v4.runtime.misc.NotNull;

public class ChangeDetailsRequest {

  @NotNull
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
