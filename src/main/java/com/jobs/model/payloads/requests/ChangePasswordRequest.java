package com.jobs.model.payloads.requests;

import org.antlr.v4.runtime.misc.NotNull;

public class ChangePasswordRequest {

  @NotNull
  private String currentPassword;
  @NotNull
  private String newPassword;

  public String getCurrentPassword() {
    return currentPassword;
  }

  public void setCurrentPassword(String currentPassword) {
    this.currentPassword = currentPassword;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }
}
