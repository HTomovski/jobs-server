package com.jobs.model.payloads.responses;

import com.jobs.model.User;
import java.util.Collections;import java.util.List;

public class UsersListResponse {
  public Long totalElements;
  public Integer totalPages;
  public List<User> users;

  public UsersListResponse(Long totalElements, Integer totalPages, List<User> users) {
    this.totalElements = totalElements;
    this.totalPages = totalPages;
    this.users = users;
  }

  public Long getTotalElements() {
    return totalElements;
  }

  public void setTotalElements(Long totalElements) {
    this.totalElements = totalElements;
  }

  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }

  public List<User> getUsers() {
    return Collections.unmodifiableList(users);
  }

  public void addUser(User user) {
    this.users.add(user);
  }
}
