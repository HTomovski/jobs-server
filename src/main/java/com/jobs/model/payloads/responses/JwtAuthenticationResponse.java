package com.jobs.model.payloads.responses;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtAuthenticationResponse {
  //    @Value("${app.jwtExpirationInMs}")
  //    private int jwtExpirationInMs;
  private String accessToken;
  private String tokenType = "Bearer";
  private long expiresAt;

  public JwtAuthenticationResponse(String accessToken, int jwtExpirationInMs) {
    this.accessToken = accessToken;
    //        System.out.println("jwtExpInMs: " + jwtExpirationInMs);
    //        System.out.println(Instant.now().toEpochMilli());
    //        System.out.println(Instant.now().toEpochMilli() + jwtExpirationInMs);
    this.expiresAt = Instant.now().plusMillis(jwtExpirationInMs).toEpochMilli();
  }
}
