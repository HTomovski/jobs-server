package com.jobs.model.payloads.responses;


public class UserSummary {
  public Long id;
  public String name;
  public String email;
  public String role;
}
